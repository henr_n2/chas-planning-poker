# Chas planning poker

## Technical choises

### Create react app

I chose to use react becouse I'm more use to it and I think it's very good to create easy webapps. The state is easy to use and I like more to program everything in components. Create react app is also easy to set up and handels most of the struggle with webpack and babel & polyfill.

### styled-components

Styled component is cool becouse you can have the styling in the same file as the react components. It creates a neat and tidy folder structure. You can also use props to inform the styling of the components. With styled components you can also use templates to have all your colors gathered in a theme file.

### react-swipeable

I used a react library to handle the mobile swiping interactions that exists in React Native. I could have created a custom function that uses mouse events but time limit made it mor eoptimal to use an existing library for it.

### prettier with pre-commit hook

I have become used to prettier to format the code. It is more usefull when there are more developers coding in the same files. Using husky you can also set prettier formating when you commit files with git.

## What i would do if I had more time

The styling is ok and I like it but I could have used more time to really make it better. I would also have liked to make it really mobile friendly with better swipe events.

import React, { Component } from 'react'
import styled from 'styled-components'

const InstructionsContainer = styled.div`
   background: ${props => props.theme.instructionsBackground};
   width: 100%;
   margin: 0 auto;
   overflow: hidden;
   .instructions-header {
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
      padding: 0 10%;
      h2 {
         color: ${props => props.theme.instructionsColor};
      }
   }
   .instructions-wrapper {
      height: auto;
      max-height: 0;
      transform-origin: left;
      transition: max-height 400ms ease-in;
      &.open {
         max-height: 600px;
      }
   }
   .instructions-list {
      width: 80%;
      padding: 60px 0;
      margin: 0 auto;
      list-style: square;
      li {
         color: ${props => props.theme.instructionsColor};
      }
      @media (min-width: 920px) {
         font-size: 14px;
         width: 60%;
      }
   }
`

class Instructions extends Component {
   render() {
      // 'open' boolean to show or hide the instruction list. Passed to the instruction-wrapper div.
      const { open } = this.props
      return (
         <InstructionsContainer open={open}>
            <div className="instructions-header" />
            <div className={`instructions-wrapper ${open ? 'open' : 'closed'}`}>
               <ul className="instructions-list">
                  <li>
                     Each individual lays a card face down representing their
                     estimate for the story. Units used vary - they can be days
                     duration, ideal days or story points. During discussion,
                     numbers must not be mentioned at all in relation to feature
                     size to avoid anchoring.
                  </li>
                  <li>
                     Everyone calls their cards simultaneously by turning them
                     over.
                  </li>
                  <li>
                     People with high estimates and low estimates are given a
                     soap box to offer their justification for their estimate
                     and then discussion continues.
                  </li>
                  <li>
                     Repeat the estimation process until a consensus is reached.
                     The developer who was likely to own the deliverable has a
                     large portion of the "consensus vote", although the
                     Moderator can negotiate the consensus.
                  </li>
               </ul>
            </div>
         </InstructionsContainer>
      )
   }
}
export default Instructions

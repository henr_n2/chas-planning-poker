import React, { Component } from 'react'
import styled from 'styled-components'

import cardImage from '../assets/playing-card-back.jpg'

const CardBackImage = styled.img`
   width: 300px;
   height: 435px;
   border-radius: 3%;
   user-drag: none;
   user-select: none;
`

class CardBack extends Component {
   render() {
      return <CardBackImage src={cardImage} />
   }
}
export default CardBack

import React, { Component } from 'react'
import styled from 'styled-components'

const FooterContainer = styled.footer`
   width: 100%;
   height: 300px;
   background-color: ${props => props.theme.footerBackgroundColor};
   display: flex;
   justify-content: center;
   align-item: center;
   .footer-inner {
      width: 80%;
      margin: 0 auto;
      h4 {
         color: ${props => props.theme.titleColor};
      }
      p {
         padding: 4px 20px;
         background-color: ${props => props.theme.textBackgroundColor};
         color: ${props => props.theme.footerColor};
         &:nth-child(even) {
            transform: skew(3deg);
         }
         &:nth-child(odd) {
            transform: skew(-3deg);
         }
      }
      a {
         color: ${props => props.theme.titleColor};
         &:hover {
            color: ${props => props.theme.titleHoverColor};
         }
      }
   }
`

class Footer extends Component {
   render() {
      return (
         <FooterContainer>
            <div className="footer-inner">
               <h4>planningDeck for Chas</h4>
               <p>Created by Henrik Nilsson</p>
               <p>
                  For more about the planning, read about it here:{' '}
                  <a href="https://en.wikipedia.org/wiki/Planning_poker">
                     LINK
                  </a>
               </p>
               <p>
                  Source code here:{' '}
                  <a href="https://bitbucket.org/henr_n2/chas-planning-poker/">
                     https://bitbucket.org/henr_n2/chas-planning-poker/
                  </a>
               </p>
            </div>
         </FooterContainer>
      )
   }
}
export default Footer

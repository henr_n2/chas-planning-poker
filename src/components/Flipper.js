import React, { Component } from 'react'
import styled from 'styled-components'
import { CardBack } from './'
import Swipeable from 'react-swipeable'

const FlipperContainer = styled.div`
   perspective: 1000px;
   width: 100%;
   height: 100%;
   .swipeable {
      height: 100%;
      width: 100%;
   }
   padding: 40px 0;
`
const FlipperDiv = styled.div`
   width: 100%;
   height: 100%;
   transition: 0.6s;
   transform-style: preserve-3d;
   transform: ${props => (props.flipped ? 'rotateY(180deg)' : 'none')};
   position: relative;
   cursor: pointer;
`
const FlipperChild = styled.div`
   backface-visibility: hidden;
   width: 100%;
   height: 100%;
   position: absolute;
   top: 0;
   left: 0;
   display: flex;
   justify-content: center;
   align-items: center;
`
const Front = styled(FlipperChild)`
   z-index: 2;
   /* for firefox 31 */
   transform: rotateY(0deg);
`
const Back = styled(FlipperChild)`
   transform: rotateY(180deg);
`
class Flipper extends Component {
   /**
    * When swipe right. Flip the card facing forward.
    */
   flipActivate = () => {
      this.props.toggleFlipp(true)
   }
   /**
    * When swipe left. Flip the card facing the back.
    */
   flipDeactivate = () => {
      this.props.toggleFlipp(false)
   }
   render() {
      return (
         <FlipperContainer>
            {/* Swipeable is a library for mobile versions to add swipe events. */}
            <Swipeable
               className="swipeable"
               onSwipingRight={() => this.flipActivate()}
               onSwipingLeft={() => this.flipDeactivate()}
            >
               <FlipperDiv
                  flipped={this.props.flipped}
                  onClick={() => this.props.toggleFlipp(!this.props.flipped)}
               >
                  <Front>
                     {/* The image for the back of the card. */}
                     <CardBack />
                  </Front>
                  {/* The Card that is the children for the component. */}
                  <Back>{this.props.children}</Back>
               </FlipperDiv>
            </Swipeable>
         </FlipperContainer>
      )
   }
}
export default Flipper

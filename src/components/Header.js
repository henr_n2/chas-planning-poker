import React, { Component } from 'react'
import styled from 'styled-components'
import downChevron from '../assets/down-chevron.svg'
import upChevron from '../assets/up-chevron.svg'

const HeaderContainer = styled.header`
   width: 100%;
   height: 60px;
   background-color: ${props => props.theme.headerBackgroundColor};
   z-index: 999;
   display: flex;
   justify-content: space-between;
   align-items: center;
   padding: 0 5%;
   .header-right {
      display: flex;
      align-items: center;
      h1 {
         margin: 0 10px;
      }
   }
`
const OpenCloseButton = styled.button`
   height: 44px;
   border: none;
   background: none;
   cursor: pointer;
   &:focus {
      outline: none;
   }
   img {
      height: 24px;
   }
`
const HeaderTitle = styled.h1`
   font-size: 20px;
   margin: 0;
   color: ${props => props.theme.titleColor};
`
class Header extends Component {
   render() {
      // Open is the boolean to show if the chevron should be up or down.
      // toggleOpen is the function to pass the opasite value to to change the state in app.
      const { open, toggleOpen } = this.props
      return (
         <HeaderContainer>
            <HeaderTitle>planningDeck</HeaderTitle>
            <div className="header-right">
               <HeaderTitle>RULES</HeaderTitle>
               <OpenCloseButton onClick={() => toggleOpen(!open)}>
                  {open ? (
                     <img src={upChevron} alt="chevron up" />
                  ) : (
                     <img src={downChevron} alt="chevron down" />
                  )}
               </OpenCloseButton>
            </div>
         </HeaderContainer>
      )
   }
}
export default Header

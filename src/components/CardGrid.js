import React, { Component } from 'react'
import styled from 'styled-components'

const Grid = styled.div`
   width: 90%;
   max-width: 1200px;
   margin: 0 auto;
   padding: 20px 0 40px 0;
   display: grid;
   grid-template-columns: repeat(2, 1fr);
   grid-gap: 10px;
   grid-auto-rows: minmax(100px, auto);
   @media (min-width: 768px) {
      grid-template-columns: repeat(3, 1fr);
   }
`

class CardGrid extends Component {
   render() {
      return <Grid>{this.props.children}</Grid>
   }
}
export default CardGrid

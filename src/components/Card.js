import React, { Component } from 'react'
import styled from 'styled-components'
import chasLoggo from '../assets/ChasLogoSvg.svg'
import coffeeIcon from '../assets/coffee-cup.svg'

// Card sections
const CardInner = styled.div`
   width: ${props => (props.full ? '300px' : '160px')};
   height: ${props => (props.full ? '435px' : '230px')};
   margin: 0 auto;
   border-radius: 10px;
   background: ${props => props.theme.cardBackgroundColor};
   box-shadow: -1px 2px 13px 1px rgba(204, 202, 204, 1);
   mix-blend-mode: multiply;
   font-family: 'Audiowide';
   @media (max-width: 320px) {
      width: ${props => (props.full ? '300px' : '123px')};
      height: ${props => (props.full ? '435px' : '178px')};
   }
`
const CardInnerTop = styled.div`
   width: 100%;
   height: 25%;
   position: relative;
   .coffee {
      height: 30px;
   }
`
const CardInnerBottom = styled.div`
   width: 100%;
   height: 25%;
   position: relative;
   .coffee {
      height: 30px;
   }
`
// The inner line for the card border
const CardOutline = styled.div`
   border: 1px solid #000;
   border-radius: 10px;
   height: 92%;
   width: 90%;
   margin: 5%;
   position: relative;
   top: ${props => (props.full ? '0' : '4%')};
`
// Value numbers
const CardTopNumber = styled.div`
   width: ${props => (props.full ? '66px' : '44px')};
   height: ${props => (props.full ? '66px' : '44px')};
   display: flex;
   justify-content: center;
   align-items: center;
   font-size: ${props => (props.full ? '24px' : '16px')};
   position: absolute;
   left: 0;
   top: 0;
`
const CardBottomNumber = styled.div`
   transform: rotate(-180deg);
   width: ${props => (props.full ? '66px' : '44px')};
   height: ${props => (props.full ? '66px' : '44px')};
   display: flex;
   justify-content: center;
   align-items: center;
   font-size: ${props => (props.full ? '24px' : '16px')};
   position: absolute;
   right: 0;
   bottom: 0;
`
const CentralNumberContainer = styled.div`
   width: 100%;
   height: 50%;
   display: flex;
   flex-direction: column;
   justify-content: center;
   align-items: center;
   font-size: ${props => (props.full ? '110px' : '60px')};
   @media (max-width: 320px) {
      font-size: ${props => (props.full ? '110px' : '40px')};
   }
   .chasLoggo {
      height: ${props => (props.full ? '32px' : '16px')};
      &--bottom {
         transform: rotate(-180deg);
      }
      @media (max-width: 320px) {
         height: ${props => (props.full ? '32px' : '10px')};
      }
   }
   .coffee {
      height: 80px;
      margin: 14px 0;
   }
`

class Card extends Component {
   render() {
      // 'value' is the string to show on the card face. 'coffee' will have a special image.
      // 'full' is a boolean if it is to have the larger styling in the selected view.
      const { value, full } = this.props
      return (
         <CardInner full={full}>
            <CardOutline full={full}>
               <CardInnerTop>
                  <CardTopNumber full={full}>
                     {value === 'coffee' ? (
                        <img className="coffee" src={coffeeIcon} alt="coffee" />
                     ) : (
                        <span>{value}</span>
                     )}
                  </CardTopNumber>
               </CardInnerTop>
               <CentralNumberContainer full={full}>
                  <img className="chasLoggo" src={chasLoggo} alt="chas" />
                  {value === 'coffee' ? (
                     <img className="coffee" src={coffeeIcon} alt="coffee" />
                  ) : (
                     <span>{value}</span>
                  )}
                  <img
                     className="chasLoggo chasLoggo--bottom"
                     src={chasLoggo}
                     alt="chas"
                  />
               </CentralNumberContainer>
               <CardInnerBottom>
                  <CardBottomNumber full={full}>
                     {value === 'coffee' ? (
                        <img className="coffee" src={coffeeIcon} alt="coffee" />
                     ) : (
                        <span>{value}</span>
                     )}
                  </CardBottomNumber>
               </CardInnerBottom>
            </CardOutline>
         </CardInner>
      )
   }
}
export default Card

import React, { Component } from 'react'
import styled, { ThemeProvider } from 'styled-components'

import './App.css'
import { theme } from './theme'
import {
   Header,
   Footer,
   CardGrid,
   Card,
   Instructions,
   Flipper,
} from './components'

// App styled components used in this file
const Main = styled.main`
   background: ${props => props.theme.mainBackgroundColor};
   transition: background-color 200ms ease;
`
// Grid card figure & figcapture.
const CardFigure = styled.figure`
   position: relative;
`
const CardFigcaption = styled.figcaption`
   width: 100%;
   height: 100%;
   position: absolute;
   left: 0;
   top: 3%;
`
const CardFigcaptionInner = styled.figcaption`
   width: 160px;
   height: 230px;
   margin: 0 auto;
   border-radius: 10px;
   background-color: ${props => props.theme.highlightBackground};
   opacity: ${props => (props.show ? '1' : '0')};
   transition: opacity 200ms ease-in;
   cursor: pointer;
   display: flex;
   justify-content: center;
   align-items: center;
   flex-direction: column;
   @media (max-width: 320px) {
      width: 123px;
      height: 178px;
   }
`
const CardButton = styled.button`
   width: 80px;
   height: 80px;
   border: none;
   border-radius: 50px;
   background-color: ${props => props.theme.selectButtonBackground};
   color: ${props => props.theme.selectButtonColor};
   cursor: pointer;
   z-index: 999;
   transition: all 200ms ease;
   cursor: pointer;
   &:focus {
      outline: none;
   }
   &:active {
      width: 100px;
      height: 100px;
   }
`
const CardHighlightButton = styled(CardButton)`
   display: ${props => (props.hide ? 'block' : 'none')};
`
// Selected view.
const SelectedView = styled.div`
   position: fixed;
   bottom: -100%;
   left: 0;
   height: 100%;
   width: 100%;
   transition: all 400ms ease-in;
   transform-origin: bottom;
   transform: ${props =>
      props.open != null ? 'translateY(-100%);' : 'translateY(0);'}
   display: flex;
   flex-direction: column;
   z-index: 98;
   background-color: rgba(0,0,0,0.7);

`
const SelectedCard = styled.div`
   display: ${props => (props.open ? 'flex' : 'none;')};
   justify-content: center;
   align-items: center;
   z-index: 99;
   width: 80%;
   height: 100%;
   margin: 10px auto;
`
const SelectedViewBottom = styled.div`
   z-index: 99;
   width: 100%;
   height: 20%;
   margin: 0 0 30px 0;
   display: flex;
   flex-direction: column;
   justify-content: center;
   align-items: center;
`
const SelectedCardText = styled.p`
   color: #fff;
   margin: 10px 0;
   text-shadow: 1px 1px 22px #000;
   @media (min-width: 920px) {
      display: none;
   }
`

class App extends Component {
   constructor(props) {
      super(props)
      // The state of the app chared wit hsome of the components.
      // 'selectedValue' is the value of the card that is to be show in the selected view that pops up.
      // 'cards' is an array of th card items that will be rendered in the card grid.
      // 'instructionsOpen' is a boolean to indicate if the instructions list is to be shown.
      // 'flipped' is a boolean to indicate if the card has been flipped over.
      this.state = {
         selectedValue: null,
         cards: [
            { status: 0, value: '0' },
            { status: 0, value: '1/2' },
            { status: 0, value: '1' },
            { status: 0, value: '2' },
            { status: 0, value: '3' },
            { status: 0, value: '5' },
            { status: 0, value: '8' },
            { status: 0, value: '13' },
            { status: 0, value: '20' },
            { status: 0, value: '40' },
            { status: 0, value: '100' },
            { status: 0, value: '?' },
            { status: 0, value: 'coffee' },
         ],
         instructionOpen: false,
         flipped: false,
      }
   }
   /**
    * Setting the new state for the instructionOpen to either close or open it.
    * @param {int} newState - The new boolean for the instructionOpen.
    */
   instructionsToggleOpen = newState => {
      this.setState({ ...this.state, instructionOpen: newState })
   }
   /**
    * Going through all the cards to set that card as highlighted with the status value.
    * @param {string} value - The value of the card that is to be highlighted.
    */
   cardToggleHighlight = value => {
      const newCards = this.state.cards

      newCards.map(card => {
         if (card.value === value) {
            let newValue
            if (card.status) {
               newValue = 0
            } else {
               newValue = 1
            }
            card.status = newValue
         } else {
            card.status = 0
         }
      })
      this.setState({ ...this.state, cards: newCards })
   }
   /**
    * Remove the highlight status for all the card in the cards array.
    */
   cancelHighlight = () => {
      const newCards = this.state.cards

      newCards.map(card => {
         card.status = 0
      })
      this.setState({ ...this.state, cards: newCards })
   }
   /**
    * Set the selected card to be shown in the selected view.
    * @param {string} newValue - The value that is the card to be selected.
    */
   cardSelection = newValue => {
      this.state.selectedValue = newValue
   }
   /**
    * Cancel the current selected card by first flipping it over to show the back and then setting the selectedValue to null.
    */
   cancelSelection = () => {
      this.setState({ ...this.state, flipped: false })
      setTimeout(() => {
         this.setState({ ...this.state, selectedValue: null })
      }, 100)
   }
   /**
    * Set weather the card will be flipped over.
    * @param {int} newStatus - The boolean if the card is flipped over or not.
    */
   toggleFlipp = newStatus => {
      this.setState({ ...this.state, flipped: newStatus })
   }
   render() {
      return (
         /*
            Themeprovider comes with styled coponents and makes it so you can easaly change colors in the Theme.js file.
          */
         <ThemeProvider theme={theme}>
            <div className="App">
               <Header
                  open={this.state.instructionOpen}
                  toggleOpen={this.instructionsToggleOpen}
               />
               {/* The instructions list that can be droped down from the button in the header.  */}
               <Instructions open={this.state.instructionOpen} />
               <Main open={this.state.selectedValue}>
                  <CardGrid>
                     {this.state.cards.map(card => {
                        return (
                           <CardFigure>
                              {/* The 'Card' component that shows the front side of the card. In the grid it is shown not in the full styling but in the smaller size. */}
                              <Card key={card.value} value={card.value} />
                              <CardFigcaption>
                                 <CardFigcaptionInner
                                    onClick={() => {
                                       this.cardToggleHighlight(card.value)
                                    }}
                                    show={card.status}
                                 >
                                    <CardHighlightButton
                                       hide={card.status}
                                       onClick={() => {
                                          this.cardSelection(card.value)
                                       }}
                                    >
                                       Select Card
                                    </CardHighlightButton>
                                 </CardFigcaptionInner>
                              </CardFigcaption>
                           </CardFigure>
                        )
                     })}
                  </CardGrid>
               </Main>
               {/* The view that pops up from bellow when you have selected a card. */}
               <SelectedView open={this.state.selectedValue}>
                  <SelectedCard open={this.state.selectedValue}>
                     {/* 'Flipped' is a component that has the backside of the card an sets weather the card is flipped over or not. */}
                     <Flipper
                        flipped={this.state.flipped}
                        toggleFlipp={this.toggleFlipp}
                     >
                        <Card full={true} value={this.state.selectedValue} />
                     </Flipper>
                  </SelectedCard>
                  <SelectedViewBottom>
                     <SelectedCardText>
                        Swipe right to turn the card over
                     </SelectedCardText>
                     <CardButton
                        onClick={() => {
                           this.cancelSelection()
                        }}
                     >
                        Cancel
                     </CardButton>
                  </SelectedViewBottom>
               </SelectedView>
               {/* Footer with links and credits */}
               <Footer />
            </div>
         </ThemeProvider>
      )
   }
}

export default App

export const theme = {
   sky: '#CAEBF2',
   carbon: '#A9A9A9',
   watermelon: '#FF3B3F',
   neatral: '#EFEFEF',
   mainBackgroundColor: '#EFEFEF',
   headerBackgroundColor: '#FFF',
   cardBackgroundColor: '#f6f4f0',
   instructionsBackground: '#FF3B3F',
   instructionsColor: '#fff',
   highlightBackground: 'rgba(43, 47, 47, 0.2)',
   selectButtonBackground: '#FF3B3F',
   selectButtonColor: '#FFF',
   titleColor: '#FF3B3F',
   titleHoverColor: '#cc1216',
   footerBackgroundColor: '#FFF',
   footerColor: '#041229',
   textBackgroundColor: '#CAEBF2',
}
